// Location

google.maps.event.addDomListener(window, 'load', function() {
    var map = new google.maps.Map(document.getElementById('map_canvas_msk'), {
        zoom: 16,
		scrollwheel: false,
        center: new google.maps.LatLng(55.712332,37.6666334),
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var infoWindow = new google.maps.InfoWindow;

    var onMarkerClick = function() {
        var marker = this;

        var latLng = marker.getPosition();
        infoWindow.setContent(marker.content);

        infoWindow.open(map, marker);
    };
    google.maps.event.addListener(map, 'click', function() {
        infoWindow.close();
    });

    var iconBase = 'https://maps.google.com/mapfiles/kml/shapes/';


    var marker1 = new google.maps.Marker({
        map: map,
        icon: 'images/localmap.png',
        content: '<strong>Офис в Москве, м.Автозаводская</strong><br />' +
        '117292 ул.Велозаводская д.9, оф.7<br />' +
         'Тел.: +7 903 223-4545<br />',
        
        position: new google.maps.LatLng(55.712332,37.6666334)
    });

    google.maps.event.addListener(marker1, 'click', onMarkerClick);


    
	var mapspb = new google.maps.Map(document.getElementById('map_canvas_spb'), {
        zoom: 16,
        scrollwheel: false,
        center: new google.maps.LatLng(55.712332,37.6666334),
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var infoWindow = new google.maps.InfoWindow;

    var onMarkerClick = function() {
        var marker = this;

        var latLng = marker.getPosition();
        infoWindow.setContent(marker.content);

        infoWindow.open(mapspb, marker);
    };
    google.maps.event.addListenerspb(mapspb, 'click', function() {
        infoWindow.close();
    });

    var iconBase = 'https://maps.google.com/mapfiles/kml/shapes/';


    var marker2 = new google.maps.Marker({
        map: mapspb,
        icon: 'images/localmap.png',
        content: '<strong>Офис в Москве, м.Автозаводская</strong><br />' +
        '117292 ул.Велозаводская д.9, оф.7<br />' +
        'Тел.: +7 903 223-4545<br />',

        position: new google.maps.LatLng(55.712332,37.6666334)
    });

    google.maps.event.addListener(marker2, 'click', onMarkerClick);

});

