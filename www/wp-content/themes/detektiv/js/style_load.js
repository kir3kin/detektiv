
var head = document.getElementsByTagName('head')[0];

var links = [
	// ["https://uslugi-detektiv.ru/wp-content/themes/detektiv/css/rwdgrid.css", ""],
	["https://uslugi-detektiv.ru/wp-content/themes/detektiv/css/responsive.css", ""],
	// ["https://fonts.googleapis.com/css?family=PT+Serif:400,400italic,700,700italic&subset=latin,cyrillic-ext,cyrillic", ""],
	["https://uslugi-detektiv.ru/wp-admin/css/farbtastic.min.css", "farbtastic-css"],
	["https://uslugi-detektiv.ru/wp-content/plugins/contact-form-7/includes/css/styles.css", "contact-form-7-css"],
	["https://uslugi-detektiv.ru/wp-content/plugins/wp-responsive-menu/css/wprmenu.css", "wprmenu.css-css"],
	["https://fonts.googleapis.com/css?family=Open+Sans%3A400%2C300%2C600", "wprmenu-font-css"],
];

links.forEach(function(elem) {
	var link = document.createElement('link');
	link.rel = 'stylesheet';
	link.media = 'all';
	link.href = elem[0];
	if (elem[1].length !== 0) link.id = elem[1];
	head.appendChild(link);
});
