<?php
include_once(dirname(__FILE__) . '/options.php');
//require_once (TEMPLATEPATH . '/inc/payment.php'); 
require_once (TEMPLATEPATH . '/inc/usluga.php'); 
require_once (TEMPLATEPATH . '/inc/slider.php'); 
require_once (TEMPLATEPATH . '/inc/shema.php'); 
require_once (TEMPLATEPATH . '/inc/shortcode.php'); 
include_once(dirname(__FILE__) . '/inc/includeplugin.php');




/* Миниатюры Вордпресс 3 */	
add_theme_support( 'post-thumbnails' );
set_post_thumbnail_size( 155,145); 
add_image_size( 'article', 270,270,true); 
add_image_size( 'shema', 171,171,true); 
add_image_size( 'usluga', 100,100,true); 
add_image_size( 'slider', 1920,520,true); 






function noEmptyCat($output) {
	if($output == "<li>Рубрик нет</li>") $output = "";
	return $output;
	}
add_filter('wp_list_categories', 'noEmptyCat');

/* Регистрация навигационного меню Вордпресс 3 */

register_nav_menus( array(



'main' => 'Основное меню',
'bottommain' => 'Нижнее меню в шапке',
'footermain' => 'Меню в подвале'
));
function my_wp_nav_menu_args($args=''){  
	$args['container'] = '';  
	return $args;  
} // function  
add_filter( 'wp_nav_menu_args', 'my_wp_nav_menu_args' );  



/* Копирайт автора в подвале админки сайта */
function remove_footer_admin () {
 echo 'Разработка сайта -  <a href="http://www.free-lance.ru/users/darziyan" target="_blank">Monozoo</a>Monozoo
<br /> 
<span class="home"><a href="http://monozoo.ru" target="_blank">monozoo.ru</a></span> |
	<span class="free"><a href="https://plus.google.com/u/0/+MaratDarziyan" target="_blank">google</a></span> | 



';
}
add_filter('admin_footer_text', 'remove_footer_admin');


/* Логотип сайта в админке вместо дефолтного */
add_action('admin_head', 'my_custom_logo');
function my_custom_logo() {
echo '
	 <style type="text/css">
	 #header-logo { width: 24px; height: 24px;  background-image: url('.get_bloginfo('template_directory').'/images/favicon.png) !important; }
	 </style>
	 ';
	} 

/* Стилизованная страница входа в админку сайта под стиль сайта */
function custom_login_logo() {
echo '<style type="text/css">

.login h1 a { background: url('.get_bloginfo('template_directory').'/images/logo.png) no-repeat; width: 92px; height: 208px; margin: -100px auto 0; display: block; position: relative;  }
body.login { background: url('.get_bloginfo('template_directory').'/images/bg.jpg) }

.message, div.updated, .login .message {border: none; background: none;}
#backtoblog { display:none; }
.login .button-primary { background: #333; color:#fff; border: none;}
.login .button-primary:hover { background: #000}

#wpwrap #wpadminbar {background-color:: #81202a}
</style>';
}
add_action('login_head', 'custom_login_logo');

add_action( 'login_headerurl', 'my_custom_login_url' );
function my_custom_login_url() {
	return ''.get_bloginfo('url').'';
}


add_action('admin_head', 'custom_colors');
function custom_colors() {
	echo '<style type="text/css">
	#wphead{background:#81202a}
	</style>';
}



add_filter( 'avatar_defaults', 'newgravatar' );
	function newgravatar ($avatar_defaults) {
	$myavatar = get_bloginfo('template_directory') . '/images/graylogo.png';
	$avatar_defaults[$myavatar] = "Casinosru";
	return $avatar_defaults;
	}
	

/* Очистка Доски обьявлений от лишней инфо */
function clear_dash(){  
	$dash_side = &$GLOBALS['wp_meta_boxes']['dashboard']['side']['core'];  
	$dash_normal = &$GLOBALS['wp_meta_boxes']['dashboard']['normal']['core'];  
//  unset($dash_side['dashboard_quick_press']); //Быстрая публикация  
//  unset($dash_side['dashboard_recent_drafts']); //Полседние черновики  
	unset($dash_side['dashboard_primary']); //Блог WordPress  
	unset($dash_side['dashboard_secondary']); //Другие Нновости WordPress  
	unset($dash_normal['dashboard_incoming_links']); //Входящие ссылки  
//  unset($dash_normal['dashboard_right_now']); //Прямо сейчас  
	unset($dash_normal['dashboard_recent_comments']); //Последние комментарии  
	unset($dash_normal['dashboard_plugins']); //Последние Плагины  
}  
add_action('wp_dashboard_setup', 'clear_dash' ); 





/* Обрезка текста - excerpt
maxchar = количество символов.
text = какой текст обрезать (по умолчанию берется excerpt поста, если его нету, то content, если есть тег <!--more-->, то maxchar игнорируется и берется все, что до него, с сохранением HTML тегов )
save_format = Сохранять перенос строк или нет. По умолчанию сохраняется. Если в параметр указать определенные теги, то они НЕ будут вырезаться из обрезанного текста (пример: save_format=<strong><a> )
echo = выводить на экран или возвращать (return) для обработки.
П.с. Шоткоды вырезаются. Минимальное значение maxchar может быть 22.
*/

function kama_excerpt($args=''){
	global $post;
		parse_str($args, $i);
		$maxchar 	 = isset($i['maxchar']) ?  (int)trim($i['maxchar'])		: 350;
		$text 		 = isset($i['text']) ? 			trim($i['text'])		: '';
		$save_format = isset($i['save_format']) ?	trim($i['save_format'])			: false;
		$echo		 = isset($i['echo']) ? 			false		 			: true;

	if (!$text){
		$out = $post->post_excerpt ? $post->post_excerpt : $post->post_content;
		$out = preg_replace ("!\[/?.*\]!U", '', $out ); //убираем шоткоды, например:[singlepic id=3]
	}

	$out = $text.$out;
	if (!$post->post_excerpt)
	$out = strip_tags($out, $save_format);

	if ( iconv_strlen($out, 'utf-8') > $maxchar ){
	$out = iconv_substr( $out, 0, $maxchar, 'utf-8' );
	$out = preg_replace('@(.*)\s[^\s]*$@s', '\\1 ...', $out); //убираем последнее слово, ибо оно в 99% случаев неполное
	}

	if($save_format){
		$out = str_replace( "\r", '', $out );
		$out = preg_replace( "!\n\n+!", "</p><p>", $out );
		$out = "<p>". str_replace ( "\n", "<br />", trim($out) ) ."</p>";
	}

	if($echo) return print $out;
	return $out;
}

function new_excerpt_length($length) {
	return 20;
}
add_filter('excerpt_length', 'new_excerpt_length');
function new_excerpt_more($more) {
	return '...';
}
add_filter('excerpt_more', 'new_excerpt_more');


/*---------------------ID post --------------------*/

function true_id($args){
	$args['post_page_id'] = 'ID';
	return $args;
}
function true_custom($column, $id){
	if($column === 'post_page_id'){
		echo $id;
	}
}
add_filter('manage_pages_columns', 'true_id', 5);
add_action('manage_pages_custom_column', 'true_custom', 5, 2);
add_filter('manage_posts_columns', 'true_id', 5);
add_action('manage_posts_custom_column', 'true_custom', 5, 2);



/* ------------ ID рубрики в админке-------------- */
function add_columns($columns) {
	$column_id = array( 'id' => 'ID' );
	$columns = array_slice( $columns, 0, 1, true ) + $column_id + array_slice( $columns, 1, NULL, true );
	return $columns;
}
add_filter("manage_edit-category_columns", 'add_columns');
add_filter("manage_edit-uslugi_columns", 'add_columns');
add_filter("manage_edit-post_tag_columns", 'add_columns');
function fill_columns($out, $column_name, $id) {
	switch ($column_name) {
		case 'id':
			$out .= $id; 
			break;
		default:
			break;
	}
	return $out;    
}
add_filter("manage_category_custom_column", 'fill_columns', 10, 3);
add_filter("manage_uslugi_custom_column", 'fill_columns', 10, 3);
add_filter("manage_post_tag_custom_column", 'fill_columns', 10, 3);


/* ------------ Удаление ненужных полей из профиля-------------- */
function add_twitter_contactmethod( $contactmethods ) {
	unset($contactmethods['aim']);
	unset($contactmethods['jabber']);
	unset($contactmethods['yim']);
	return $contactmethods;
}

add_filter('user_contactmethods','add_twitter_contactmethod',10,1);

/* ------------ Перенос скриптв и стилей в футер -------------- */
function scripts_in_footer()
{
	wp_deregister_script( 'jquery' );
	wp_register_script( 'jquery', '/wp-includes/js/jquery/jquery.js', array(), null, true );

	wp_dequeue_script( 'my_script' );
	wp_enqueue_script( 'my_script', plugin_dir_url(__FILE__).'template/template1Script.js', array('jquery'), null, true );

	wp_dequeue_script( 'jquery.transit' );
	wp_dequeue_script( 'sidr');
	wp_dequeue_script( 'wprmenu.js' );

	wp_enqueue_script( 'jquery.transit', plugins_url( '/js/jquery.transit.min.js', __FILE__ ), array( 'jquery' ), null, true );
	wp_enqueue_script( 'sidr', plugins_url( '/js/jquery.sidr.js', __FILE__ ), array( 'jquery' ), null, true );
	wp_enqueue_script( 'wprmenu.js', plugins_url( '/js/wprmenu.js', __FILE__ ), array( 'jquery' ), null, true );
	wp_enqueue_script( 'jquery-migrate', '/wp-includes/js/jquery/jquery-migrate.min.js', array( 'jquery' ), null, true );
}

function styles_in_footer()
{
	wp_dequeue_style( 'wprmenu.css' );
	wp_dequeue_style( 'wprmenu-font' );
	wp_dequeue_style( 'contact-form-7' );
	wp_dequeue_style( 'farbtastic' );
	wp_register_script( 'style_load', get_template_directory_uri() . '/js/style_load.js', array(), null, true );
	wp_enqueue_script( 'style_load' );
}

add_action( 'wp_enqueue_scripts', 'styles_in_footer' );
add_action( 'wp_enqueue_scripts', 'scripts_in_footer' );

/* ------------ Удаление ненужных ссылок на каждую статью https://exmaple.ru/.../feed/-------------- */
remove_action( 'wp_head', 'feed_links_extra', 3 );