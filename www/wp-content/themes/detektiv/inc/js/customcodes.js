(function() {
    
	
	tinymce.create('tinymce.plugins.column', {
        init : function(ed, url) {
            ed.addButton('column', {
                title : 'Колонки',
                image : url+'/column.png',
                onclick : function() {
                     ed.selection.setContent('[column col=6]' + ed.selection.getContent() + '[/column]');  
 
                }
            });
        },
        createControl : function(n, cm) {
            return null;
        },
    });
    tinymce.PluginManager.add('column', tinymce.plugins.column);
	
	
	
	
	//
	
		tinymce.create('tinymce.plugins.columninner', {
        init : function(ed, url) {
            ed.addButton('column_inner', {
                title : 'Вложенные колонки',
                image : url+'/column_inner.png',
                onclick : function() {
                     ed.selection.setContent('[column_inner col=6]' + ed.selection.getContent() + '[/column_inner]');  
 
                }
            });
        },
        createControl : function(n, cm) {
            return null;
        },
    });
    tinymce.PluginManager.add('column_inner', tinymce.plugins.columninner);
	
	
	
	
	//
	
	tinymce.create('tinymce.plugins.clear', {
        init : function(ed, url) {
            ed.addButton('clear', {
                title : 'Обнулить колонки',
                image : url+'/clear.png',
                onclick : function() {
                     ed.selection.setContent('[clear]');  
 
                }
            });
        },
        createControl : function(n, cm) {
            return null;
        },
    });
    tinymce.PluginManager.add('clear', tinymce.plugins.clear);
	
	//
	
	tinymce.create('tinymce.plugins.faq', {
        init : function(ed, url) {
            ed.addButton('faq', {
                title : 'FAQ',
                image : url+'/faq.png',
                onclick : function() {
                     ed.selection.setContent('[faq title="Ваш вопрос"]' + ed.selection.getContent() + '[/faq]');  
 
                }
            });
        },
        createControl : function(n, cm) {
            return null;
        },
    });
    tinymce.PluginManager.add('faq', tinymce.plugins.faq);
	
	
	tinymce.create('tinymce.plugins.spoiler', {
        init : function(ed, url) {
            ed.addButton('spoiler', {
                title : 'Спойлер',
                image : url+'/spoilerbottom.png',
                onclick : function() {
                     ed.selection.setContent('[spoiler name="Подробнее"]' + ed.selection.getContent() + '[/spoiler]');  
 
                }
            });
        },
        createControl : function(n, cm) {
            return null;
        },
    });
     tinymce.PluginManager.add('spoiler', tinymce.plugins.spoiler);
	 
	 
	 tinymce.create('tinymce.plugins.call', {
        init : function(ed, url) {
            ed.addButton('call', {
                title : 'Спойлер',
                image : url+'/call.png',
                onclick : function() {
                     ed.selection.setContent('[call text="Заказать звонок"]');  
 
                }
            });
        },
        createControl : function(n, cm) {
            return null;
        },
    });
     tinymce.PluginManager.add('call', tinymce.plugins.call);
	
})();