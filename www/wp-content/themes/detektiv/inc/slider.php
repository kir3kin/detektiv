<? 
add_action( 'init', 'register_cpt_slider' );

function register_cpt_slider() {

    $labels = array( 
        'name' => _x( 'Слайды', 'slider' ),
        'singular_name' => _x( 'Слайд', 'slider' ),
        'add_new' => _x( 'Добавить новый слайд', 'slider' ),
        'add_new_item' => _x( 'Добавить новый слайд', 'slider' ),
        'edit_item' => _x( 'Редактировать слайд', 'slider' ),
        'new_item' => _x( 'Новый слайд', 'slider' ),
        'view_item' => _x( 'Просмотр слайда', 'slider' ),
        'search_items' => _x( 'Поиск слайдера', 'slider' ),
        'not_found' => _x( 'Не найдено', 'slider' ),
        'not_found_in_trash' => _x( 'Корзина пуста', 'slider' ),
        'parent_item_colon' => _x( 'Родительский слайд', 'slider' ),
        'menu_name' => _x( 'Слайдер', 'slider' ),
    );

    $args = array( 
        'labels' => $labels,
        'hierarchical' => false,
        
        'supports' => array( 'title', 'editor', 'thumbnail' ),
        
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 20,
        'menu_icon' => ''.get_template_directory_uri().'/images/slide.png',
        'show_in_nav_menus' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'has_archive' => false,
        'query_var' => true,
        'can_export' => true,
        'rewrite' => true,
        'capability_type' => 'post'
    );

    register_post_type( 'slider', $args );
}

add_action('publish_slider', 'add_custom_field_automatically');
function add_custom_field_automatically($post_ID) {
	global $wpdb;
	if(!wp_is_post_revision($post_ID)) {
		add_post_meta($post_ID, 'slyder', 'on', true);
		
	}
}