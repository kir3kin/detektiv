<? 
add_action( 'init', 'register_cpt_shema' );

function register_cpt_shema() {

    $labels = array( 
        'name' => _x( 'Схема работы', 'shema' ),
        'singular_name' => _x( 'Схема работы', 'shema' ),
        'add_new' => _x( 'Добавить пункт схемы', 'shema' ),
        'add_new_item' => _x( 'Добавить пункт схемы', 'shema' ),
        'edit_item' => _x( 'Редактировать пункт схемы', 'shema' ),
        'new_item' => _x( 'Новый пункт схемы', 'shema' ),
        'view_item' => _x( 'Просмотр пункт схемы', 'shema' ),
        'search_items' => _x( 'Поиск пункт схемы', 'shema' ),
        'not_found' => _x( 'Не найдено', 'shema' ),
        'not_found_in_trash' => _x( 'Корзина пуста', 'shema' ),
        'parent_item_colon' => _x( 'Parent Схема работы :', 'shema' ),
        'menu_name' => _x( 'Схема работы', 'shema' ),
    );

    $args = array( 
        'labels' => $labels,
        'hierarchical' => false,
        
        'supports' => array( 'title', 'thumbnail' ),
        
        'public' => false,
        'show_ui' => true,
        
        
        
        'show_in_nav_menus' => true,
        'publicly_queryable' => false,
        'exclude_from_search' => true,
        'has_archive' => false,
        'query_var' => true,
        'can_export' => true,
        'rewrite' => true,
        'capability_type' => 'post'
    );

    register_post_type( 'shema', $args );
}