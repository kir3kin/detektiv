<?php
/**
 * This file represents an example of the code that themes would use to register
 * the required plugins.
 *
 * It is expected that theme authors would copy and paste this code into their
 * functions.php file, and amend to suit.
 *
 * @package    TGM-Plugin-Activation
 * @subpackage Example
 * @version    2.4.0
 * @author     Thomas Griffin <thomasgriffinmedia.com>
 * @author     Gary Jones <gamajo.com>
 * @copyright  Copyright (c) 2014, Thomas Griffin
 * @license    http://opensource.org/licenses/gpl-2.0.php GPL v2 or later
 * @link       https://github.com/thomasgriffin/TGM-Plugin-Activation
 */

/**
 * Include the TGM_Plugin_Activation class.
 */
require_once dirname( __FILE__ ) . '/class-tgm-plugin-activation.php';

add_action( 'tgmpa_register', 'my_theme_register_required_plugins' );
/**
 * Register the required plugins for this theme.
 *
 * In this example, we register two plugins - one included with the TGMPA library
 * and one from the .org repo.
 *
 * The variable passed to tgmpa_register_plugins() should be an array of plugin
 * arrays.
 *
 * This function is hooked into tgmpa_init, which is fired within the
 * TGM_Plugin_Activation class constructor.
 */
function my_theme_register_required_plugins() {

    /**
     * Array of plugin arrays. Required keys are name and slug.
     * If the source is NOT from the .org repo, then source is also required.
     */
    $plugins = array(

        			
				array(
					'name' 				=> 'Cyr to Lat enhanced',
					'slug' 				=> 'cyr3lat',
					'required'			=> true,
					'force_activation' 	=> false,
					'force_deactivation'=> false,
				),
				
				array(
					'name' 				=> 'Advanced Custom Fields',
					'slug' 				=> 'advanced-custom-fields',
					'required'			=> true,
					'force_activation' 	=> false,
					'force_deactivation'=> false,
				),
				
				
				
				array(
					'name' 				=> 'WordPress SEO',
					'slug' 				=> 'wordpress-seo',
					'required'			=> false,
					'force_activation' 	=> false,
					'force_deactivation'=> false,
				),				
	
				array(
					'name' 				=> 'Auto Post Thumbnail',
					'slug' 				=> 'auto-post-thumbnail',
					'required'			=> false,
					'force_activation' 	=> false,
					'force_deactivation'=> false,
				),
				

    );

    /**
     * Array of configuration settings. Amend each line as needed.
     * If you want the default strings to be available under your own theme domain,
     * leave the strings uncommented.
     * Some of the strings are added into a sprintf, so see the comments at the
     * end of each line for what each argument will be.
     */
    $config = array(
        'default_path' => '',                      // Default absolute path to pre-packaged plugins.
        'menu'         => 'tgmpa-install-plugins', // Menu slug.
        'has_notices'  => true,                    // Show admin notices or not.
        'dismissable'  => true,                    // If false, a user cannot dismiss the nag message.
        'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
        'is_automatic' => false,                   // Automatically activate plugins after installation or not.
        'message'      => '',                      // Message to output right before the plugins table.
        'strings'      => array(
            'page_title'                      => __( 'Установить Требуемые Плагины', 'tgmpa' ),
            'menu_title'                      => __( 'Установить плагины', 'tgmpa' ),
            'installing'                      => __( 'Установка плагина: %s', 'tgmpa' ), // %s = plugin name.
            'oops'                            => __( 'Что-то пошло не так с plugin API.', 'tgmpa' ),
            'notice_can_install_required'     => _n_noop( 'Эта тема требует следующих плагинов: %1$s.', 'Эта тема требует следующие плагины: %1$s.' ), // %1$s = plugin name(s).
            'notice_can_install_recommended'  => _n_noop( 'Эта тема рекомендует следующий плагин: %1$s.', 'Эта тема рекомендует следующие плагины: %1$s.' ), // %1$s = plugin name(s).
            'notice_cannot_install'           => _n_noop( 'Извините, но у вас нет необходимых разрешений для установки плагина %s. Обратитесь к администратору этого сайта за помощью.', 'Извините, но у вас нет необходимых разрешений для установки %s плагинов. Обратитесь к администратору этого сайта за помощью.' ), // %1$s = plugin name(s).
            'notice_can_activate_required'    => _n_noop( 'Следующий обязательный плагин в данный момент неактивен: %1$s.', 'Следующие необходимые плагины неактивные: %1$s.' ), // %1$s = plugin name(s).
            'notice_can_activate_recommended' => _n_noop( 'Рекомендуемый плагин в данный момент неактивен: %1$s.', 'Рекомендуемые плагины в данный момент не активны: %1$s.' ), // %1$s = plugin name(s).
            'notice_cannot_activate'          => _n_noop( 'Извините, но у вас нет необходимых разрешений для активации плагина %s. Обратитесь к администратору этого сайта за помощью.', 'Извините, но у вас нет необходимых разрешений для активации плагинов %s. Обратитесь к администратору этого сайта за помощью.' ), // %1$s = plugin name(s).
            'notice_ask_to_update'            => _n_noop( 'Этот плагин должен быть обновлен до последней версии, чтобы гарантировать максимальную совместимость с этой темой: %1$s.', 'Следующие плагины должны быть обновлены до последней версии, чтобы гарантировать максимальную совместимость с этой темой: %1$s.' ), // %1$s = plugin name(s).
            'notice_cannot_update'            => _n_noop( 'Извините, но у вас нет необходимых разрешений для обновления плагина %s. Обратитесь к администратору этого сайта за помощью', 'Извините, но у вас нет необходимых разрешений для обновления плагинов %s. Обратитесь к администратору этого сайта за помощью.' ), // %1$s = plugin name(s).
            'install_link'                    => _n_noop( 'Начать установку плагина', 'Начать установку плагинов' ),
            'activate_link'                   => _n_noop( 'Активировать плагин', 'Активировать плагины' ),
            'return'                          => __( 'Вернуться в установщик плагинов', 'tgmpa' ),
            'plugin_activated'                => __( 'Плагин успешно активирован.', 'tgmpa' ),
            'complete'                        => __( 'Все плагины установлены и успешно активирован. %s', 'tgmpa' ), // %s = dashboard link.
            'nag_type'                        => 'updated' // Determines admin notice type - can only be 'updated', 'update-nag' or 'error'.
        )
    );

    tgmpa( $plugins, $config );

}