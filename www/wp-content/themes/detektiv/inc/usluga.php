<?

add_action( 'init', 'register_cpt_usluga' );

function register_cpt_usluga() {

    $labels = array( 
        'name' => _x( 'Услуги', 'usluga' ),
        'singular_name' => _x( 'Услугу', 'usluga' ),
        'add_new' => _x( 'Добавить новую услугу', 'usluga' ),
        'add_new_item' => _x( 'Добавить новую услугу', 'usluga' ),
        'edit_item' => _x( 'Редактировать услугу', 'usluga' ),
        'new_item' => _x( 'Новая услуга', 'usluga' ),
        'view_item' => _x( 'Просмотр услуги', 'usluga' ),
        'search_items' => _x( 'Поиск услуг', 'usluga' ),
        'not_found' => _x( 'Не найдено', 'usluga' ),
        'not_found_in_trash' => _x( 'Корзина пуста', 'usluga' ),
        'parent_item_colon' => _x( 'Родительская услуга', 'usluga' ),
        'menu_name' => _x( 'Услуги', 'usluga' ),
    );

    $args = array( 
        'labels' => $labels,
        'hierarchical' => true,
        
        'supports' => array( 'title', 'editor', 'thumbnail', 'uslugi', 'custom-fields', 'comments' ),
        
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 5,
        'menu_icon' => ''.get_template_directory_uri().'/images/low.png',
        'show_in_nav_menus' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'has_archive' => false,
        'query_var' => true,
        'can_export' => true,
        'rewrite' => true,
        'capability_type' => 'post'
    );

    register_post_type( 'usluga', $args );
}

add_action( 'init', 'register_taxonomy_uslugi' );

function register_taxonomy_uslugi() {

    $labels = array( 
        'name' => _x( 'Рубрики услуг', 'uslugi' ),
        'singular_name' => _x( 'Рубрика услуг', 'uslugi' ),
        'search_items' => _x( 'Поиск по рубрикам', 'uslugi' ),
        'popular_items' => _x( 'Популярные рубрики', 'uslugi' ),
        'all_items' => _x( 'Все рубрики услуг', 'uslugi' ),
        'parent_item' => _x( 'Родительская рубрика услуг', 'uslugi' ),
        'parent_item_colon' => _x( 'Родительская рубрика:', 'uslugi' ),
        'edit_item' => _x( 'Редактировать рубрику', 'uslugi' ),
        'update_item' => _x( 'Обновить рубрику', 'uslugi' ),
        'add_new_item' => _x( 'Добавить новую рубрику услуг', 'uslugi' ),
        'new_item_name' => _x( 'Новая рубрика услуг', 'uslugi' ),
        'separate_items_with_commas' => _x( 'Разделите рубрики услуг запятой', 'uslugi' ),
        'add_or_remove_items' => _x( 'Добавить или удалить рубрику услуг', 'uslugi' ),
        'choose_from_most_used' => _x( 'Используйте самые популярные рубрики услуг', 'uslugi' ),
        'menu_name' => _x( 'Рубрики услуг', 'uslugi' ),
    );

    $args = array( 
         'labels' => $labels,
        'public' => true,
        'show_in_nav_menus' => true,
        'show_ui' => true,
        'show_tagcloud' => true,
        'hierarchical' => true,

        'rewrite' => true,
        'query_var' => true
    );

    register_taxonomy( 'uslugi', array('usluga'), $args );
}