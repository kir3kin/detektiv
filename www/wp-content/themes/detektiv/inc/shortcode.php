<? 

add_shortcode( 'column', 'column_shortcode' );
add_shortcode( 'column_inner', 'column_shortcode' );
add_shortcode( 'clear', 'clear_floats_shortcode' );
add_shortcode( 'faq', 'faq_shortcode' );
add_shortcode('spoiler', 'hyper_spoiler');
add_shortcode('call', 'call_shortcode');

function column_shortcode($atts, $content = null) {
extract(shortcode_atts(array(
"col" => '6'
), $atts));
return '<div class="grid-'.$col.'">'.do_shortcode($content).'</div>';
}

 function clear_floats_shortcode() {
extract(shortcode_atts(array(), $atts));
return '<div class="break"></div>';
}

function call_shortcode($atts, $content = null) {
extract(shortcode_atts(array(
"text" => 'Заказать звонок'
), $atts));
return '<a onclick="$("#zvonok").arcticmodal()" class="more">'.$text.'</a>';
}

 function faq_shortcode($atts, $content = null) {
extract(shortcode_atts(array(
"title" => 'Текст вопроса'
), $atts));
return '<div class="grid-12 faq"><div class="title">'.$title.'</div><div class="answer">'.$content.'</div></div>';
}

function hyper_spoiler($atts, $content) {
if (!isset($atts[name])) {$sp_name = 'Спойлер';}
else {$sp_name = $atts[name];}
return '<div class="spoiler-wrap">
<div class="spoiler-head folded">'.$sp_name.'</div>
<div class="spoiler-body">'.$content.'</div>
</div>';}

add_action('admin_footer', 'eg_quicktags');
function eg_quicktags() {
    ?>
    <script type="text/javascript" charset="utf-8">
        jQuery(document).ready(function(){
            if(typeof(QTags) !== 'undefined') {
			
				QTags.addButton( 'column', 'Колонки', '[column col=6]\n', '\n[/column]');				
				QTags.addButton( 'column_inner', 'Вложенные колонки', '[column_inner col=6]\n', '\n[/column_inner]');		
				QTags.addButton( 'clear', 'Обнулить', '[clear]');				
                QTags.addButton( 'faq', 'FAQ', '[faq title="Ваш вопрос"]\n', '\n[/faq]'); 
				QTags.addButton( 'spoiler', 'Спойлер', '[spoiler name="Подробнее"]\n', '\n[/spoiler]');				
				QTags.addButton( 'call', 'Заказать звонок', '[call text="Заказать звонок"]');				
                
              

                
            }
        });
    </script>
    <?php
}
add_action('init', 'add_button');

function add_button() {
   if ( current_user_can('edit_posts') &&  current_user_can('edit_pages') )
   {
     add_filter('mce_external_plugins', 'add_plugin');
     add_filter('mce_buttons', 'register_button');
   }
}

function register_button($buttons) {

   array_push($buttons, "column");
   array_push($buttons, "column_inner");
   array_push($buttons, "clear");
   array_push($buttons, "faq");
   array_push($buttons, "spoiler");
   array_push($buttons, "call");
  
  
   return $buttons;
}

function add_plugin($plugin_array) {
     
     $plugin_array['column'] = get_bloginfo('template_url').'/inc/js/customcodes.js';   
     $plugin_array['column_inner'] = get_bloginfo('template_url').'/inc/js/customcodes.js';   
     $plugin_array['clear'] = get_bloginfo('template_url').'/inc/js/customcodes.js';
     $plugin_array['faq'] = get_bloginfo('template_url').'/inc/js/customcodes.js';
     $plugin_array['spoiler'] = get_bloginfo('template_url').'/inc/js/customcodes.js';
     $plugin_array['call'] = get_bloginfo('template_url').'/inc/js/customcodes.js';
    
    
   return $plugin_array;
}