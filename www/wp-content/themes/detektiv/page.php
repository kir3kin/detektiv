<?php get_header(); ?>
<section class="screen_03 singlepost">
    <div class="container" itemscope itemprop="articleSection">
	 <?php
			if (have_posts()) :
			while (have_posts()) : the_post(); 
			$arc_year = get_the_time('Y');
			$arc_month = get_the_time('m');
			$arc_day = get_the_time('d');
			?>
        <article class="grid-12" itemscope itemprop="articleBody">
        <h1 itemscope itemprop="name"><?php the_title(); ?></h1>
			<?php if ( function_exists('yoast_breadcrumb') ) { yoast_breadcrumb(' <p>','</p>'); } ?>
       	<?php the_content(); ?> 
        </article>
	<?php endwhile; ?><?php endif; ?>	 
    </div>
</section>
<?php include (TEMPLATEPATH . '/footer.php');  ?>