<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7">
<![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8">
<![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9">
<![endif]-->
<!--[if gt IE 8]><!--> 
<html class="no-js">
<!--<![endif]-->
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php wp_title(''); ?></title>
<meta name="viewport" content="width=device-width">
<link rel="stylesheet" href="<?php bloginfo("template_url"); ?>/style.css">
<!-- соеденил с style.css
<link rel="stylesheet" href="<?php bloginfo("template_url"); ?>/css/rwdgrid.css"> 
<link href='https://fonts.googleapis.com/css?family=PT+Serif:400,400italic,700,700italic&subset=latin,cyrillic-ext,cyrillic' rel='stylesheet' type='text/css'> -->
<!-- <link rel="stylesheet" href="<?php bloginfo("template_url"); ?>/css/responsive.css"> -->
<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<link rel="icon" href="<?php bloginfo("url"); ?>/favicon.ico" type="image/x-icon">
<link rel="shortcut icon" href="<?php bloginfo("url"); ?>/favicon.ico" type="image/x-icon">
<?php wp_head(); ?>
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
  (function (d, w, c) {
      (w[c] = w[c] || []).push(function() {
          try {
              w.yaCounter32707810 = new Ya.Metrika({id:32707810,
                      webvisor:true,
                      clickmap:true,
                      trackLinks:true,
                      accurateTrackBounce:true});
          } catch(e) { }
      });
      var n = d.getElementsByTagName("script")[0],
          s = d.createElement("script"),
          f = function () { n.parentNode.insertBefore(s, n); };
      s.type = "text/javascript";
      s.async = true;
      s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";
      if (w.opera == "[object Opera]") {
          d.addEventListener("DOMContentLoaded", f, false);
      } else { f(); }
  })(document, window, "yandex_metrika_callbacks");
</script>
<noscript>
  <div><img src="//mc.yandex.ru/watch/32707810" style="position:absolute;left:-9999px;" alt="" /></div>
</noscript>
<!-- /Yandex.Metrika counter -->
</head>
<body <?php body_class(); ?>>
<?php  global $NHP_Options; ?>
<?php $docs = $NHP_Options->get("docs"); ?>
<?php $phone_01 = $NHP_Options->get("phone_01"); ?>
<?php $phone_02 = $NHP_Options->get("phone_02"); ?>
<?php $phone_03 = $NHP_Options->get("phone_03"); ?>
<?php $mobphone = $NHP_Options->get("mobphone"); ?>
<header itemscope itemtype="http://schema.org/WPHeader">
  <div class="container">
    <div class="grid-3">
      <a href="<?php bloginfo("url"); ?>" class="logo"><img src="<?php bloginfo("template_url"); ?>/images/logo.jpg" alt="Детективное агентство в Москве и СПБ"></a>
      <p><strong>Детективное агентство</strong></p>
      <?  if ($docs) { ?>
      <p class="license">Лицензия МВД<br>№<? echo $docs ?></p>
      <? } ?>
   </div>
   <div class="grid-3" style="margin-top:-22px;margin-left:320px;width:21.7%;">
<!-- <div class="grid-3" style="margin-top:-22px;margin-left:368px;width: 16.7%;"> -->
      <p class="local" itemprop="addressLocality">Россия, г. Москва</p>
      <p class="local" itemprop="addressLocality">Россия, г. Санкт-Петербург</p>
      <p class="local" itemprop="addressLocality">Россия, Республика Крым</p>
   </div>
  <div class="grid-3" itemscope itemtype="http://schema.org/Organization" style="margin-top:-18px;">
    <p class="phone01">
      <span itemprop="telephone"><? echo $phone_01 ?></span><br/>
      <span itemprop="telephone"><? echo $phone_02 ?></span><br/>
      <span itemprop="telephone"><? echo $phone_03 ?></span>
    </p>
  </div>
  <div class="clear-grid"></div>
   <nav id="topmenu">
    <? if (has_nav_menu('main')){
       wp_nav_menu( array(
       		'container' => '',
       		'depth' => 1,
       		'theme_location' => 'main')
       ); } else { ?>
    <ul>
       <li class="sitemap"><a href="#">Карта сайта</a></li>
       <li class="cur"><a href="#">Нанять детектива</a> </li>
       <li><a href="#">Услуги</a> </li>
       <li><a href="#">Гарантии</a> </li>
       <li><a href="#">Цены</a> </li>
       <li><a href="#">Вопрос-ответ</a> </li>
       <li><a href="#">Полезные сервисы</a> </li>
       <li><a href="#">Контакты</a> </li>
    </ul>
    <? } ?>
   </nav>
  </div>
  <div id="bottommenu">
    <div class="container">
      <? if (has_nav_menu('bottommain')){ 
         wp_nav_menu( array(
         		'container' => '',
         		'depth' => 1,
         		'theme_location' => 'bottommain')
         ); } else { ?>
      <ul>
        <li class="fiz"><a href="#">Физическим лицам</a> </li>
        <li class="yur"><a href="#">Юридическим лицам</a> </li>
        <li class="org"><a href="#">Организациям</a> </li>
        <li class="exp"><a href="#">Экспертиза</a> </li>
      </ul>
      <? } ?>
    </div>
  </div>
</header>
<!-- .header-->