<?php
/*
Template Name: Главная
*/
?>
<?php get_header(); ?>
<section id="slide">
<ul class="homeslider">
 <?php
$slider = new WP_Query('post_type=slider'); while($slider->have_posts()){ $slider->the_post(); ?>
<? $large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full'); ?>
  <li style=" background-image: url(<? echo $large_image_url[0]; ?>);">
    <div class="container">
      <p class="title"><?php the_title(); ?></p>
     <?php the_content(); ?> 
    </div>
  </li>
  <? } wp_reset_postdata(); ?>
</ul>
</section>
<section class="screen_02" itemscope itemtype="http://schema.org/Article" >
  <div class="romb"></div>
  <div class="container">
    <div class="grid-8" itemscope itemprop="articleSection">
      <article itemscope itemprop="articleBody" >
     <?php
        if (have_posts()) :
        while (have_posts()) : the_post(); 
        $arc_year = get_the_time('Y');
        $arc_month = get_the_time('m');
        $arc_day = get_the_time('d');
        ?>
      <h1 itemscope itemprop="name"><?php the_title(); ?></h1>
      <?php the_content(); ?> 
      <?php endwhile; ?><?php endif; ?>
      </article>
    </div>
    <div class="grid-4"></div>
    <div class="clear-grid"></div>
  </div>
</section>
<section class="screen_03">
  <div class="container" itemscope itemtype="http://schema.org/Product">
    <h2 class="grid-12">Услуги</h2>
    <?php $fizlica = $NHP_Options->get("fizlica");  if ($fizlica) { ?>
    <div class="grid-6">
      <h3 class="fizlica">Частным лицам</h3>
      <ul class="fizul" >
<?php
$fizusl = new WP_Query('posts_per_page=10&order=ASC&meta_key=sortusluga&orderby=meta_value_num&uslugi='.$fizlica); while($fizusl->have_posts()){ $fizusl->the_post();
$thumb_name = get_the_post_thumbnail_caption();
$thumb_title = get_the_title(get_post_thumbnail_id()); ?>
        <li><div><a itemscope itemprop="url" href="<?php the_permalink(); ?>"><?php if(has_post_thumbnail()) { ?><span itemscope itemprop=" image"><? the_post_thumbnail('usluga', "title=$thumb_title&name=$thumb_name"); ?></span> <span itemscope itemprop="name"><?php the_title(); ?></span><? } ?></a></div></li>
               <? } wp_reset_postdata(); ?>
      </ul>
    </div>
    <? } ?>
    <?php $yurlica = $NHP_Options->get("yurlica");  if ($yurlica) { ?>
    <div class="grid-6">
      <h3 class="yurlica">Юридическим лицам</h3>
      <ul class="fizul">
       <?php  
$yurusl = new WP_Query('posts_per_page=10&order=ASC&meta_key=sortusluga&orderby=meta_value_num&uslugi='.$yurlica); while($yurusl->have_posts()){ $yurusl->the_post();
$thumb_name = get_the_post_thumbnail_caption();
$thumb_title = get_the_title(get_post_thumbnail_id()); ?>
        <li itemscope itemtype="http://schema.org/Offer"><div><a itemscope itemprop="url" href="<?php the_permalink(); ?>"><?php if(has_post_thumbnail()) { ?><span itemscope itemprop=" image"><? the_post_thumbnail('usluga', "title=$thumb_title&name=$thumb_name"); ?></span><span itemscope itemprop="name"> <?php the_title(); ?></span><? } ?></a></div></li>
       <? } wp_reset_postdata(); ?>
      </ul>
    </div>
    <? } ?>
    <div class="clear-grid"></div>
    <?php $pages_select = $NHP_Options->get("pages_select");  if ($pages_select) { ?>
    <noindex> <p align="center"><a href="/?p=<? echo $pages_select ?>" class="more">Все услуги</a> </p></noindex>
    <? } ?>
  </div>
</section>
<section class="screen_04">
  <div class="dubleromb"></div>
  <div class="container" itemscope itemprop="articleBody">
    <?php the_field('bottomtext'); ?>
  </div>
</section>
<?php get_footer(); ?>