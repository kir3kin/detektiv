<?php get_header(); ?>	

<section class="screen_03 singlepost">
    <div class="container">
	 <article class="grid-8 mobile-view" itemscope itemtype="http://schema.org/Offer">
        <h1 itemscope itemprop="name"><?php the_title(); ?></h1>
		<?php if ( function_exists('yoast_breadcrumb') ) { yoast_breadcrumb(' <p>','</p>'); } ?>
	 <?php
			if (have_posts()) :
			while (have_posts()) : the_post(); 
			$arc_year = get_the_time('Y');
			$arc_month = get_the_time('m');
			$arc_day = get_the_time('d');
			?>	
			   
			
       	<?php the_content(); ?> 
        
	<?php endwhile; ?><?php endif; ?>	 
	</article>

	 <div class="grid-2 mobile-view">
	 
	 <noindex>
	 <? $term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) ); ?>
<p><strong>
<?php
$taxonomy = 'uslugi';
$queried_term = get_query_var($taxonomy);
$terms = get_the_terms($post->id, $taxonomy);
if ($terms) {
  foreach($terms as $term) {
    echo $term->name . " ";
  }
}
?></strong></p>

<?php 
 
// get the custom post type's taxonomy terms
 
$custom_taxterms = wp_get_object_terms( $post->ID, 'uslugi', array('fields' => 'ids') );
// arguments
$args = array(
'post_type' => 'usluga',
'post_status' => 'publish',
'posts_per_page' => -1, // you may edit this number
'orderby' => 'rand',
'tax_query' => array(
    array(
        'taxonomy' => 'uslugi',
        'field' => 'id',
        'terms' => $custom_taxterms
    )
),
'post__not_in' => array ($post->ID),
);
$related_items = new WP_Query( $args );
// loop over query
if ($related_items->have_posts()) : ?>


<ul>

<? while ( $related_items->have_posts() ) : $related_items->the_post();
?>
    <li><a itemscope itemprop="url" href="<?php the_permalink(); ?>"><span itemscope itemprop="name"><?php the_title(); ?></span></a></li>
<?php
endwhile;
echo '</ul>';
endif;
// Reset Post Data
wp_reset_postdata();
?>
      </noindex>      
        </div>
			
	 <div class="grid-2 shemabl"><noindex>
		<?php  
$shema = new WP_Query('post_type=shema&posts_per_page=-1&order=ASC'); while($shema->have_posts()){ $shema->the_post(); ?>
            <p align="center" class="shemadiv post_<?php the_ID(); ?>"><? the_post_thumbnail('shema'); ?> <br><?php the_title(); ?><br><a onclick="$('#zvonok').arcticmodal()" class="more">Отправить заявку</a></p>
          <? } wp_reset_postdata(); ?></noindex>
        </div>
		
    </div>
</section>

		

		<?php include (TEMPLATEPATH . '/footer.php');  ?>