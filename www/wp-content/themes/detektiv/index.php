<?php get_header(); ?>	
<section class="screen_03 singlepost">
    <div class="container">
		<div class="grid-12">	
				<?php $post = $posts[0]; // Hack. Set $post so that the_date() works. ?>
				
<?php /* If this is a category archive */ if (is_category()) { ?>

<h1 ><?php single_cat_title(); ?></h1>
 <?php /* If this is a tag archive */ } elseif( is_tag() ) { ?>
<h1><span><?php single_tag_title(); ?></span></h1>
<?php /* If this is a daily archive */ } elseif (is_day()) { ?>
<h1><span><?php _e('Archive for','ps'); ?><?php the_time('j F Y'); ?></span></h1>
<?php /* If this is a monthly archive */ } elseif (is_month()) { ?>
<h1><span><?php _e('Archive for','ps'); ?><?php the_time('F, Y'); ?></span></h1>
<?php /* If this is a yearly archive */ } elseif (is_year()) { ?>
<h1><span><?php _e('Archive for','ps'); ?><?php the_time('Y'); ?> <?php _e('year','ps'); ?></span></h1>
<?php /* If this is an author archive */ } elseif (is_author()) { ?>
<h1><span><?php _e('Autor Archive','ps'); ?></span></h1>
<?php /* If this is a paged archive */ } elseif (isset($_GET['paged']) && !empty($_GET['paged'])) { ?>
<h1><span><?php _e('Blog Archive','ps'); ?></span></h1>
<?php /* If this is a yearly archive */ } elseif (is_search()) { ?>
<h1><span><?php _e('Search resultates by','ps'); ?>: <?php the_search_query(); ?> </span></h1>
<?php } ?>            


  <div class="break"></div> 
   <?php if ( function_exists('yoast_breadcrumb') ) { yoast_breadcrumb(' <p>','</p>'); } ?>
	</div>
<?php		if (have_posts()) :
			while (have_posts()) : the_post(); 
			$arc_year = get_the_time('Y');
			$arc_month = get_the_time('m');
			$arc_day = get_the_time('d');
$thumb_name = get_the_post_thumbnail_caption();
$thumb_title = get_the_title(get_post_thumbnail_id());
			?>	
			
			<article class="grid-12 article">
			<?php if(has_post_thumbnail()) { ?><a itemscope itemprop=" image" href="<?php the_permalink(); ?>"><? the_post_thumbnail('article', array('class' => 'alignleft', 'title' => $thumb_title, 'name' => $thumb_name)); ?></a><? } ?>
			<h2 itemscope itemprop="name"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
            <p itemscope itemprop="description"><?php kama_excerpt("maxchar=700"); ?></p>
            <a itemscope itemprop="url" href="<?php the_permalink(); ?>">Подробнее</a>
            <div class="break"></div>
        </article>
				
			
		
				 
            <?php endwhile; ?>
	  <div class="break"></div>


	<?php if(function_exists('wp_pagenavi')) { ?> <div id="pagenavi"><? wp_pagenavi(); ?></div><? } ?>


    <?php

    function wp_corenavi() {
        global $wp_query;
        $pages = '';
        $max = $wp_query->max_num_pages;
        if (!$current = get_query_var('paged')) $current = 1;
        $a['base'] = str_replace(999999999, '%#%', get_pagenum_link(999999999));
        $a['total'] = $max;
        $a['current'] = $current;

        $total = 1; //1 - выводить текст "Страница N из N", 0 - не выводить
        $a['mid_size'] = 3; //сколько ссылок показывать слева и справа от текущей
        $a['end_size'] = 1; //сколько ссылок показывать в начале и в конце
        $a['prev_text'] = '&laquo;'; //текст ссылки "Предыдущая страница"
        $a['next_text'] = '&raquo;'; //текст ссылки "Следующая страница"

        if ($max > 1) echo '<div class="navigation">';
        if ($total == 1 && $max > 1) $pages = '<span class="pages">Страница ' . $current . ' из ' . $max . '</span>'."\r\n";
        echo $pages . paginate_links($a);
        if ($max > 1) echo '</div>';
    }

    ?>

    <?php if (function_exists('wp_corenavi')) wp_corenavi(); ?>

		<? else : ?>


    <?php

    $page_links[] = '<a class="prev page-numbers" href="' . esc_url( apply_filters( 'paginate_links', $link ) ) . '">' . $args['prev_text'] . '</a>';
    $page_links[] = "<a class='page-numbers' href='" . esc_url( apply_filters( 'paginate_links', $link ) ) . "'>" . $args['before_page_number'] . number_format_i18n( $n ) . $args['after_page_number'] . "</a>";
    $page_links[] = '<a class="next page-numbers" href="' . esc_url( apply_filters( 'paginate_links', $link ) ) . '">' . $args['next_text'] . '</a>';
    ?>
<article>
Раздел в стадии разработки
</article>
		<?php endif; ?>	
         
    </div>
</section>
		<?php include (TEMPLATEPATH . '/footer.php');  ?>