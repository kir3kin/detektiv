  <section id="map">
    <div class="section" >
    <?php global $NHP_Options; ?>
      <div class="box visible">
        <div itemscope itemprop="foundingLocation" id="map_canvas_msk" style="background-image: url(<?php echo $NHP_Options->get('mskmap'); ?>)"></div>
      </div>
      <div class="box">
        <div itemscope itemprop="foundingLocation" id="map_canvas_spb"  style="background-image: url(<?php echo $NHP_Options->get('spbmap'); ?>)"></div>
      </div>
      <div class="container">
        <ul class="tabs">
          <li class="current">Офис в Москве (Россия)</li>
          <li id="create">Офис в Санкт-Петербурге (Россия)</li>
        </ul>
  <?php $formbottom = $NHP_Options->get("formbottom"); if ($formbottom) { ?>
        <div id="formblock">
          <h2>Доверьтесь нам</h2>
          <?php  echo do_shortcode('[contact-form-7 id="'.$formbottom.'"]'); ?>
        </div>
          <? } ?>
      </div>
    </div><!--End tabs-->
  </section>
  <footer>
    <div class="container"  itemscope itemtype="http://schema.org/Organization">
  <?php $docs = $NHP_Options->get("docs"); ?>
  <?php $phone_01 = $NHP_Options->get("phone_01"); ?>
  <?php $phone_02 = $NHP_Options->get("phone_02"); ?>
  <?php $phone_03 = $NHP_Options->get("phone_03"); ?>
  <?php $mobphone = $NHP_Options->get("mobphone"); ?>
      <div class="grid-4">
        <p itemprop="streetAddress"><?php echo $NHP_Options->get('adresmsk'); ?></p>
        <p itemprop="streetAddress"><?php echo $NHP_Options->get('adresspb'); ?></p>
        <p class="copy"><?php echo $NHP_Options->get('copy'); ?></p>
      </div>
      <div class="grid-4">
        <? if (has_nav_menu('main')){ 
      wp_nav_menu( array(  
              'container' => '',  
              'depth' => 1,
              'theme_location' => 'footermain')  
      ); } ?>
      </div>
      <div class="grid-4">
        <h3>Наши контакты</h3>
        <div class="grid-6"><p itemprop="telephone"><? echo $phone_01 ?><br/><? echo $phone_02 ?></p></div>
        <div class="grid-6"> <p itemprop="telephone"><? echo $mobphone ?><br/><? echo $phone_03 ?></p></div>
      </div>
    </div>
  </footer>
  <?php wp_footer(); ?>
  <!-- ============= Модальное окно заказа звонка ============= -->
  <div style="display: none;"><div class="box-modal" id="zvonok">
    <div class="box-modal_close arcticmodal-close"></div>
    <p class="title">Заказать звонок</p>
  <form action="https://wap.zadarma.com/43df0f9217" method="get" id="zcbForm"> 
    Мой номер: <input type="text" name="n" id="phone"  maxlength="32" /> 
    <input name="" type="submit" value="Перезвоните мне" /> 
  </form> 
  <div id="zcbFormAnswer" style="display: none;"></div>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script> 
  <script>
    $(document).ready(function(){
      var formElement = $('#zcbForm');
      if (formElement.length > 0) {
        formElement.on('submit', function(){
          $.ajax({
            url: formElement.attr('action'),
            cache: false,
            data: formElement.serialize() + '&data_type=jsonp',
            dataType: 'jsonp',
            type: 'GET',
            beforeSend: function(){
              $('#zcbForm').hide();
              $('#zcbFormAnswer').html('...');
              $('#zcbFormAnswer').show();
            }
          });
          return false;
        });
      }
    });
  </script>
  </div></div>
  <script src="<?php bloginfo("template_url"); ?>/js/jquery.bxslider.js"></script>
  <script src="<?php bloginfo("template_url"); ?>/js/tabs.js"></script>
  <script src="<?php bloginfo("template_url"); ?>/js/modal.js"></script>
  <script src="<?php bloginfo("template_url"); ?>/js/jquery.maskedinput.js"></script>
  <script src="<?php bloginfo("template_url"); ?>/js/script.js"></script>
  <?php echo $NHP_Options->get('footerscript'); ?>
</body>
</html>