<?php
/*
Template Name: Архив услуг
*/
?>

<?php get_header(); ?>	


<section class="screen_03 singlepost">
    <div class="container">
	
	 <?php
			if (have_posts()) :
			while (have_posts()) : the_post(); 
			$arc_year = get_the_time('Y');
			$arc_month = get_the_time('m');
			$arc_day = get_the_time('d');
			?>	
			
        <article class="grid-12">

        <h1><?php the_title(); ?></h1>
			<?php if ( function_exists('yoast_breadcrumb') ) { yoast_breadcrumb(' <p>','</p>'); } ?>
       	<?php the_content(); ?> 
        </article>
	<?php endwhile; ?><?php endif; ?>	 

	<?php $fizlica = $NHP_Options->get("fizlica");  if ($fizlica) { ?>
		<div class="grid-6">
            <h3 class="fizlica">Частным лицам</h3>
            <ul class="fizul" >
			 <?php  
$fizusl = new WP_Query('posts_per_page=-1&order=ASC&meta_key=sortusluga&orderby=meta_value_num&uslugi='.$fizlica); while($fizusl->have_posts()){ $fizusl->the_post(); 
$thumb_name = get_the_post_thumbnail_caption();
$thumb_title = get_the_title(get_post_thumbnail_id()); ?>
                <li itemscope itemtype="http://schema.org/Offer"><div><a itemscope itemprop="url" href="<?php the_permalink(); ?>"><?php if(has_post_thumbnail()) { ?><span itemscope itemprop=" image"><? the_post_thumbnail('usluga', "title=$thumb_title&name=$thumb_name"); ?></span> <span itemscope itemprop="name"><?php the_title(); ?></span><? } ?></a></div></li>
               <? } wp_reset_postdata(); ?>
            </ul>
        </div>
		<? } ?>
		
		<?php $yurlica = $NHP_Options->get("yurlica");  if ($yurlica) { ?>
        <div class="grid-6">
            <h3 class="yurlica">Юридическим лицам</h3>
            <ul class="fizul">
               <?php  
$yurusl = new WP_Query('posts_per_page=-1&order=ASC&meta_key=sortusluga&orderby=meta_value_num&uslugi='.$yurlica); while($yurusl->have_posts()){ $yurusl->the_post(); 
$thumb_name = get_the_post_thumbnail_caption();
$thumb_title = get_the_title(get_post_thumbnail_id()); ?>
                <li itemscope itemtype="http://schema.org/Offer"><div><a itemscope itemprop="url" href="<?php the_permalink(); ?>"><?php if(has_post_thumbnail()) { ?><span itemscope itemprop=" image"><? the_post_thumbnail('usluga', "title=$thumb_title&name=$thumb_name"); ?></span> <span itemscope itemprop="name"><?php the_title(); ?></span><? } ?></a></div></li>
               <? } wp_reset_postdata(); ?>
            </ul>
        </div>
		<? } ?>
        <div class="clear-grid"></div>
        <div class="btn-group-uslug">
	        <div class="grid-6">
<p style="text-align: center;"><a onclick="$('#zvonok').arcticmodal()" class="more">Позвонить детективу</a></p>
</div>

        <div class="grid-6">
<p style="text-align: center;"><a class="more" href="/tsenoobrazovanie/">Ценообразование</a></p>
</div></div>
		<?php the_field('bottomtext'); ?>

    </div>
</section>

		

		<?php include (TEMPLATEPATH . '/footer.php');  ?>